package com.example.djpra.myapplication;

import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Constraints;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button button;
    SeekBar seekBarR,seekBarG,seekBarB;
    Integer Red,Green,Blue;
    TextView textView;
    String Red1,Green1,Blue1;
    ConstraintLayout li;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
         li = (ConstraintLayout)findViewById(R.id.id1);
        button = (Button)findViewById(R.id.button);
        textView = findViewById(R.id.textView2);
        seekBarR = (SeekBar)findViewById(R.id.seekBarR);
        seekBarG = (SeekBar)findViewById(R.id.seekBarG);
        seekBarB = (SeekBar)findViewById(R.id.seekBarB);
        seekBarR.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                Red1=String.valueOf(progress);
                Red = Integer.parseInt(Red1);

            }


            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        seekBarG.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Green1=String.valueOf(progress);
                Green = Integer.parseInt(Green1);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        seekBarB.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Blue1=String.valueOf(progress);
                Blue = Integer.parseInt(Blue1);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),Red.toString(),Toast.LENGTH_LONG).show();
                li.setBackgroundColor(Color.rgb(Red,Green,Blue));
            }
        });


    }

}
